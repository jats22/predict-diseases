import React, { Component } from 'react';

class TeamMemUploadImage extends Component{

  propTypes(){
    value: React.PropTypes.object.isRequired
  }

  handleDocumentTitleChange(evt){
      // Need fileName to update file Uploaded fully!
      this.props.onUpload(evt.target.value.slice(12),this.props.value);
  }

  render() {
    // console.log("In TeamMemUploadImage.js \n \n ",this.props)
    //const percentComplete = this.props.value + '%';
    return (<div className="fileinput fileinput-new" data-provides="fileinput">
      <a href="#" className="btn-file">
      {"Choose file"}
      <input type="file" ref={(f)=>{this.file=f}} onChange={this.handleDocumentTitleChange.bind(this)}/>
      </a>
      <b className="fileinput-filename">{}</b>
    </div>);
  }
}

export default TeamMemUploadImage;
